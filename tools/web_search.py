import re
from typing import List, Dict, Optional
import config

def simplify_search_results(search_results: List[Dict]) -> List[Dict]:
    """
    Упрощает результаты поиска, оставляя только основную информацию.

    Args:
        search_results (List[Dict]): Список результатов поиска.

    Returns:
        List[Dict]: Упрощенный список результатов.
    """
    simplified_results = []
    for result in search_results:
        simplified_result = {
            "title": result.get("title"),
            "link": result.get("link")
        }
        simplified_results.append(simplified_result)
    return simplified_results

def web_search_tool(query: str) -> List[Dict]:
    """
    Инструмент для выполнения веб-поиска с использованием SerpAPI.
    
    Args:
        query (str): Поисковый запрос.
        
    Returns:
        List[Dict]: Список результатов поиска.
    """
    if not config.SERPAPI_API_KEY:
        print("SerpAPI key is not configured.")
        return []

    params = {
        "engine": "google",
        "q": query,
        "api_key": config.SERPAPI_API_KEY,
    }

    search_results = requests.get("https://serpapi.com/search", params=params).json().get('organic_results', [])
    simplified_results = simplify_search_results(search_results)
    return simplified_results

# Метаинформация
tool_info = {
    "name": "web-search",
    "operation": web_search_tool,
    "arguments": [
        ("query", "task")
    ],
    "prompt": "For tasks using [web-search], provide the search query, and only the search query to use (eg. not 'research waterproof shoes, but 'waterproof shoes'). Result will be a summary of relevant information from the first few articles. When requiring multiple searches, use the [web-search] multiple times. This tool will use the dependent task result to generate the search query if necessary."
}
