{ pkgs ? import <nixpkgs> {} }:

let
  pythonEnv = with pkgs; with pkgs.python3Packages; [
    python3
    tkinter
    python-dotenv
    requests
    beautifulsoup4
    openai
    simplejson
    pytest
    tiktoken
    pip
    pyyaml
  ];
in
pkgs.mkShell {
  buildInputs = pythonEnv;
}
