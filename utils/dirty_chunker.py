#!../python.sh

import tiktoken
from typing import List
from math import ceil

tenc = tiktoken.encoding_for_model("gpt-4")

def dirty_chunker_util(large_text: str, chunk_size: int = 8192) -> List[str]:
    tokens = tenc.encode(large_text)
    tlen = len(tokens)
    max_step = int(chunk_size / 2)
    extra = int(max_step / 4)
    if tlen <= chunk_size + extra:
        return [large_text]
    freeshift = tlen - (chunk_size - 2)
    step = int(min(max_step,max(max_step / 4, freeshift / ceil(freeshift / max_step))))
    out = []
    prev_cur = 0
    for i in range(0, tlen, step):
        last = False
        if tlen - i <= chunk_size:
            cur = tlen - chunk_size - 1
            end = -1
            last = True
        else:
            cur = i
            end = i + chunk_size
        chunk = tokens[cur:end]
        out.append(tenc.decode(chunk))
        prev_cur = cur
        if last:
            break
    return out

if __name__ == "__main__":
    with open("../ideas/agentic_ways_01.txt", 'r', encoding='utf-8') as file:
            content = file.read()

    #for a in dirty_chunker_util(content[0:-1600] + content[0:10000] + content[0:101]):
    for a in dirty_chunker_util(content):
        print("\n")
        print("====\n")
        print(a)
        print("\n====")
        print("\n")

