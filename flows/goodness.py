#!../python.sh

import logging
import unittest

class Cost:
    def __init__(self, value = None):
        """Initializes the cost, sets to a default high value if none provided."""
        if value is not None:
            self.value = value
        else:
            self.reset()

    def reset(self):
        """Resets the cost to a default high value of 999999."""
        logging.debug("Cost value reset to high default")  # Log when the cost is reset to default
        self.value = 999999

    # Overriding comparison operators for sorting and comparison purposes.
    def __lt__(self, other): return self.value < other.value
    def __le__(self, other): return self.value <= other.value
    def __eq__(self, other): return self.value == other.value
    def __gt__(self, other): return self.value > other.value
    def __ge__(self, other): return self.value >= other.value

class Cargo:

    def __init__(self, cost_value=None):
        self.cost = Cost(cost_value)

    def get_cost(self) -> Cost:
        """Returns the cost of the cargo."""
        return self.cost

class Improvements:

   def __init__(self, accumulate = True):
       """Initializes improvements storage."""
       self.accumulate = accumulate
       self.stack = []

   def is_empty(self):
       return len(self.stack) < 1

   def push(self, cargo: Cargo):
       """Adds a cargo to stack, either by accumulating or resetting."""
       logging.debug(f"Pushing cargo to stack with accumulate={self.accumulate}")  # Log the operation mode
       if self.accumulate:
           self.stack.append(cargo)
       else:
           # This is like out stack is only with one element, so without accumulating.
           self.stack = [cargo]

   def get_best(self):
       """Gets the best cargo, throws an error if none."""
       if self.is_empty():
           raise Exception("Improvements not exist")
       return self.stack[-1]

   def reset(self):
       """Clears all improvements."""
       logging.debug("Resetting improvements stack")  # Log when resetting the stack
       self.stack = []

   def finish(self):
       """Completes the improvements and clears the stack."""
       logging.debug("Finishing improvements and resetting stack")
       best = self.get_best()
       self.reset()
       return best

class Goodness:

    def __init__(self, target_cost = Cost(8), limit = 5):
        """Initializes goodness tracking with a target cost and limits."""

        self.target_cost = target_cost
        self.best_cost = Cost()

        self.limit = limit
        self.improvements = Improvements()
        self.goods = []

        self.good = False
        self.tries = 0
        self.goodness = 0

    def is_good(self):
        return self.good

    def has_improvements(self):
        return not self.improvements.is_empty()

    def last_improvement(self):
        return self.improvements.get_best()

    def last_good(self):
        return self.goods[-1]

    def simple_case(self, callback):
        if not self.is_good():
            callback(0, self) # Retry from start
        elif self.has_improvements():
            callback(1, self) # It is possible to get last_improvement
        else:
            callback(2, self) # Is is possible to get last_good

    def new_try(self):
        """Registers a new try, checks the limit on attempts."""
        logging.debug("Registering a new attempt")  # Log each new attempt

        if self.goodness - self.tries <= -self.limit:
            raise Exception(f"Exceeded the limit of attempts: {self.limit} tries without sufficient goodness")
        self.tries += 1

    def some_success(self, cargo: Cargo):
        """Evaluates a cargo; updates best cost if better.
    
        Raise an exception if the cargo cost is not better than the current best cost.
        It's intended to allow error handling outside and to proceed with new attempts.
        """

        logging.info(f"Trying to improve with cargo candidate: {cargo}")
        cost = cargo.get_cost()
        if cost >= self.best_cost:
            logging.error(f"Cargo cost {cost.value} is not better than current best {self.best_cost.value}")
            raise Exception("Goodness: must be lower cost")

        self.best_cost = cost
        self.improvements.push(cargo)
        if cost <= self.target_cost:
            self.best_cost.reset()
            self.goods.append(self.improvements.finish())

        self.good = True
        self.goodness += 1

class LoggingTest(unittest.TestCase):
    def run(self, result=None):
        logging.debug(f"STARTING TEST: {self.id()}")
        super().run(result)

class TestImprovementsMethods(LoggingTest):

    def test_accumulation(self):
        improvements = Improvements(accumulate=True)
        improvements.push(Cargo(100))
        improvements.push(Cargo(50))
        self.assertEqual(len(improvements.stack), 2)

    def test_no_accumulation(self):
        improvements = Improvements(accumulate=False)
        improvements.push(Cargo(100))
        improvements.push(Cargo(50))
        self.assertEqual(len(improvements.stack), 1)  # Only the last cargo should be present

class TestGoodness(LoggingTest):

    def test_basic_operations(self):
        goodness = Goodness()
        cargo = Cargo()
        goodness.new_try()
        self.assertRaises(Exception, goodness.some_success, cargo)

        cargo_low_cost = Cargo(5)
        goodness.new_try()
        goodness.some_success(cargo_low_cost)
        self.assertEqual(len(goodness.goods), 1)

    def test_attempts_limit(self):
        """Tests that an exception is raised when the limit of attempts is exceeded."""

        goodness = Goodness(limit=2)
        cargo = Cargo(cost_value=10)
    
        # Performing exactly at the limit should not raise an exception
        goodness.new_try()
        goodness.new_try()
    
        # The next try should exceed the limit and raise an exception
        with self.assertRaises(Exception) as context:
            goodness.new_try()
        self.assertIn("Exceeded the limit of attempts", str(context.exception))

    def test_best_cost_adjustment(self):
        goodness = Goodness()
        cargo = Cargo(10)
        goodness.some_success(cargo)
        self.assertEqual(goodness.best_cost.value, 10)

    def test_sequential_improvements(self):
        goodness = Goodness()
        cargo1 = Cargo(10)
        cargo2 = Cargo(9)
        goodness.some_success(cargo1)
        goodness.some_success(cargo2)
        self.assertEqual(goodness.best_cost.value, 9)

    def test_handling_cost_increase(self):
        goodness = Goodness()
        cargo = Cargo(10)
        goodness.some_success(cargo)
        with self.assertRaises(Exception):
            goodness.some_success(Cargo(11))

class TestSimpleCase(LoggingTest):

    def test_without_improvements(self):
        def callback(stage, obj):
            self.assertEqual(stage, 0)
            self.assertIs(obj, goodness)
            
        goodness = Goodness()
        goodness.simple_case(callback)

    def test_with_improvements_not_good(self):
        def callback(stage, obj):
            self.assertEqual(stage, 1)
            
        goodness = Goodness(target_cost=Cost(5))
        goodness.some_success(Cargo(7))
        goodness.simple_case(callback)

    def test_with_improvements_good(self):
        def callback(stage, obj):
            self.assertEqual(stage, 2)
            
        goodness = Goodness(target_cost=Cost(5))
        goodness.some_success(Cargo(5))
        goodness.simple_case(callback)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()

