import config
from task_manager import add_task, get_incomplete_tasks, update_task_status, task_list
from tools import tools
from concurrent.futures import ThreadPoolExecutor
import time
import os

def read_examples():
    current_file = os.path.abspath(__file__)
    base_name = os.path.splitext(os.path.basename(current_file))[0]
    folder_path = os.path.join(os.path.dirname(current_file), f"{base_name}/examples")
    all_files = os.listdir(folder_path)
    text_files = [file for file in all_files if file.endswith('.txt')]
    all_texts = ''
    for file_name in text_files:
        file_path = os.path.join(folder_path, file_name)
        with open(file_path, 'r', encoding='utf-8') as file:
            all_texts += file.read() + '\n'
    return all_texts

# Это попытка сделать подобие это функции уже в новом виде, вам нужно улучшить код
"""
def task_creation_agent(objective: str) -> List[Dict]:
    global task_list
    minified_task_list = [{k: v for k, v in task.items() if k != "result"} for task in task_list]
"""

def make_prompt(objective: str):
    examples = read_examples()
    prompt = (
        f"You are an expert task creation AI tasked with creating a  list of tasks as a JSON array, considering the ultimate objective of your team: '''```{objective}```'''.\n{examples}\n"
        f"OBJECTIVE='''```{objective}```'''\n"
        f"TASK LIST="
    )
    return prompt


def read_examples():
    # Подправил путь к текущему файлу, используя __file__ вместо необъявленной переменной file
    current_file = os.path.abspath(__file__)
    base_name = os.path.splitext(os.path.basename(current_file))[0]
    folder_path = os.path.join(os.path.dirname(current_file), f"{base_name}_examples")  # Изменил на ___examples для означения папки
    try:
        all_files = os.listdir(folder_path)
    except FileNotFoundError:
        print(f"Директория с примерами {folder_path} не найдена.")
        return ""
    text_files = [file for file in all_files if file.endswith('.txt')]
    all_texts = ''
    for file_name in text_files:
        file_path = os.path.join(folder_path, file_name)
        with open(file_path, 'r', encoding='utf-8') as file:
            all_texts += file.read() + '\n'
    return all_texts

def make_prompt(objective: str):
    examples = read_examples()
    prompt = (
        f"You are an expert task creation AI tasked with creating a list of tasks as a JSON array, considering the ultimate objective of your team: '''{objective}'''. Below are some examples of tasks you can create.\n\nExamples:\n{examples}\n"
        f"Based on the objective and examples provided, create a new list of tasks. These tasks should be detailed and diligently formulated to align with the objective: '''{objective}'''\n"
        f"TASK LIST="
    )
    return prompt

prompt = (
        f"You are an expert task creation AI tasked with creating a  list of tasks as a JSON array, considering the ultimate objective o
f your team: {OBJECTIVE}. "
        f"Create new tasks based on the objective. Limit tasks types to those that can be completed with the available tools listed belo
w. Task description should be detailed."
        f"Current tool options are [text-completion] {websearch_var} {user_input_var}." # web-search is added automatically if SERPAPI e
xists
        f"For tasks using [web-search], provide the search query, and only the search query to use (eg. not 'research waterproof shoes, 
but 'waterproof shoes'). Result will be a summary of relevant information from the first few articles."
        f"When requiring multiple searches, use the [web-search] multiple times. This tool will use the dependent task result to generat
e the search query if necessary."
        f"Use [user-input] sparingly and only if you need to ask a question to the user who set up the objective. The task description s
hould be the question you want to ask the user.')"
        f"dependent_task_ids should always be an empty array, or an array of numbers representing the task ID it should pull results fro
m."
        f"Make sure all task IDs are in chronological order.\n"
        f"EXAMPLE OBJECTIVE=Look up AI news from today (May 27, 2023) and write a poem."
        "TASK LIST=[{\"id\":1,\"task\":\"AI news today\",\"tool\":\"web-search\",\"dependent_task_ids\":[],\"status\":\"incomplete\",\"r
esult\":null,\"result_summary\":null},{\"id\":2,\"task\":\"Extract key points from AI news articles\",\"tool\":\"text-completion\",\"dep
endent_task_ids\":[1],\"status\":\"incomplete\",\"result\":null,\"result_summary\":null},{\"id\":3,\"task\":\"Generate a list of AI-rela
ted words and phrases\",\"tool\":\"text-completion\",\"dependent_task_ids\":[2],\"status\":\"incomplete\",\"result\":null,\"result_summa
ry\":null},{\"id\":4,\"task\":\"Write a poem using AI-related words and phrases\",\"tool\":\"text-completion\",\"dependent_task_ids\":[3
],\"status\":\"incomplete\",\"result\":null,\"result_summary\":null},{\"id\":5,\"task\":\"Final summary report\",\"tool\":\"text-complet
ion\",\"dependent_task_ids\":[1,2,3,4],\"status\":\"incomplete\",\"result\":null,\"result_summary\":null}]"
        f"OBJECTIVE={OBJECTIVE}"
        f"TASK LIST="
    )

def initialize_tasks():
    """
    Функция для инициализации стартового набора задач.
    """
    # Пример добавления задачи
    # Добавляем задачи в соответствии с OBJECTIVE, для начала - загрузку данных
    add_task(task_description="Что такое теория игр", tool="text_completion", dependent_task_ids=[])

def execute_task(task):
    """
    Функция для исполнения задачи в соответствии с указанным инструментом.
    
    Args:
        task (dict): Задача для исполнения.
    """
    task_id = task['id']
    print(f"Выполнение задачи {task_id}: {task['task']}")

    # Получаем конфигурацию утилиты по имени, указанному в задаче
    tool_config = tools.get(task['tool'])
    if tool_config:
        # Собираем аргументы для функции утилиты из данных задачи
        # Например, для text_completion, task["task"] будет использоваться как prompt
        #args = {arg_info[0]: task.get(arg_info[1], '') for arg_info in tool_config["arguments"]}

        # новый код, перевроверьте всё ли правильно
        args = {}
        for input_name, task_key in tool_config["arguments"]:
            value = task
            for key in task_key.split('.'):
                value = value.get(key, '')
                if value == '':
                    print(f'Не найден ключ {key} для задачи {task_id}')
                    return  # или любая другая логика обработки ошибки
            args[input_name] = value
        try:
            task_output = tool_config["operation"](**args)
            update_task_status(task_id=task_id, new_status="complete")
            task['output'] = task_output
            print(f"Задача {task_id} выполнена. Результат: {task_output}")
        except Exception as e:
            print(f"Ошибка при выполнении задачи {task_id}: {e}. Задача будет перезапущена.")
            # Здесь может быть логика по переносу задачи в конец списка или повторного выполнения позже
            # я тут подумал может просто сделать статус idle, таким образом мы другим способом ставим фоновый приоритет
            update_task_status(task_id=task_id, new_status="idle")
    else:
        print(f"Неизвестный инструмент: {task['tool']}.")

# TODO: Работа с ThreadPoolExecutor и остановка выполнения: - Ваш подход к использованию ThreadPoolExecutor для выполнения задач параллельно — это отличное решение для повышения эффективности ассистента. Однако не забудьте реализовать корректную остановку ассистента и освобождение ресурсов (например, вызов executor.shutdown()).


def process_idle_tasks(executor):
    while True:
        idle_tasks = [task for task in task_list if task["status"] == "idle"]
        if len(idle_tasks) == 0:
            break
        futures = [executor.submit(execute_task, task) for task in idle_tasks]
        time.sleep(1)
        for future in futures:
            future.result()

def run():
    """
    Основная функция для запуска AI ассистента.
    """
    # Инициализация задач
    initialize_tasks()

    # Создаем ThreadPoolExecutor для параллельного выполнения задач
    with ThreadPoolExecutor(max_workers=3) as executor:
        while True:
            incomplete_tasks = get_incomplete_tasks()
            if not incomplete_tasks:
                print("Все задачи выполнены.")
                break

            futures = [executor.submit(execute_task, task) for task in incomplete_tasks]
            # Ожидаем выполнения всех задач в пуле
            for future in futures:
                future.result()

            # После обработки невыполненных задач
            process_idle_tasks(executor)

            # Пауза перед следующей проверкой задач
            time.sleep(1)

if __name__ == "__main__":
    run()


"""
В этом коде:

1. Функция initialize_tasks определяется для начального создания задач. Здесь ты можешь добавить логику, определяющую какие задачи и в каком порядке должны быть выполнены. Пример показывает добавление одной задачи, но ты можешь расширить этот список в зависимости от ваших целей.

2. execute_task берет задачу из списка и выполняет ее, используя определенный в задаче инструмент. Результаты выполнения задач сохраняются непосредственно в объекте задачи и обновляется ее статус.

3. run – это основная функция, запускающая бесконечный цикл обработки задач. Задачи берутся из списка невыполненных, выполняются параллельно с помощью ThreadPoolExecutor и после выполнения статус задач обновляется.

Этот код представляет основную логику работы твоего AI Assistant. Он циклически проверяет список задач, исполняет их и обновляет статус. Такой подход позволяет легко масштабировать и добавлять новые функции.
"""
