from typing import List, Dict
import re

def extract_relevant_info(text: str, keywords: List[str]) -> str:
    """
    Извлекает из текста информацию, содержащую ключевые слова.
    
    Args:
        text (str): Текст для анализа.
        keywords (List[str]): Список ключевых слов.
        
    Returns:
        str: Строка с извлеченной информацией.
    """
    relevant_lines = [line for line in text.split('\n') if any(keyword in line for keyword in keywords)]
    return ' '.join(relevant_lines)

def format_as_markdown(text: str) -> str:
    """
    Форматирует текст в Markdown.
    
    Args:
        text (str): Исходный текст.
        
    Returns:
        str: Текст, отформатированный как Markdown.
    """
    # Пример простого форматирования: преобразование заголовков в Markdown-стиль
    text = re.sub('^(.+)$', '# \\1', text, flags=re.MULTILINE)
    return text

"""
В этом файле представлены примеры трех вспомогательных функций:

1. simplify_search_results() - было перенесено в tools плагин
2. extract_relevant_info() - анализирует текст и извлекает строки, содержащие определенные ключевые слова, что может быть полезно для обработки больших блоков текста.
3. format_as_markdown() - простой пример функции, которая форматирует текст, чтобы он выглядел как Markdown, что может пригодиться при подготовке отчетов или выводе информации для пользователя.

Эти функции являются лишь примерами того, как ты можешь использовать вспомогательные инструменты в своем проекте. Ты можешь добавить любое количество других утилит, которые окажутся полезными для твоей конкретной задачи.
"""
