
### Project Overview

This project is an AI assistant capable of automating the process of creating, managing, and executing tasks. Its key feature is the ability to create tasks, manage them (including adding, updating status, and listing incomplete tasks), and automatically execute them using various tools (e.g., text completion, web scraping, user input). This looks like a high-level tool that can be applied in a wide range of areas, from automating routine tasks to more complex operations that require gathering data from external sources or even generating content.

### Key Components of the Project

1. **helpers.py**: A set of auxiliary functions, in particular, for extracting information from texts by keywords and formatting text in Markdown. This can be useful for analyzing texts and preparing reports or displaying information in a convenient format.

2. **task_manager.py**: A module for task management, including adding new tasks to a list, searching for a task by ID, updating the task status, and obtaining a list of incomplete tasks. This is the heart of the task management system.

3. **ai_assistant_core.py**: The core of the AI assistant, which initiates the creation and execution of tasks using tools defined in tools (text completion, web scraping, etc.). It also contains logic for executing tasks in parallel.

4. **main.py**: The main module that starts the AI assistant, handles exceptions, and properly terminates its operation.

5. **config.py**: A configuration module storing global settings, such as API keys and target tasks for the AI assistant.

6. **tools/**: A directory with tools (text_completion, web_scrape, user_input, etc.) used to perform various tasks, for example, text generation or gathering information from the internet.

7. **agents/**: Components to extend the capabilities of the AI assistant, including creating new tasks based on a given goal.

### Goals and Capabilities

The main goal of the project is to provide a tool for automatic creation, management, and execution of tasks using AI. Thanks to its modular structure and a set of tools (tools), the AI assistant can be adapted for many different tasks and processes, simplifying routine operations and improving work efficiency.

### Usage Example

Let's describe a basic usage scenario: imagine you need to research the latest news in artificial intelligence and write a poem based on your findings in Shakespeare's style. The AI assistant automates the process of finding information, extracting key points from articles, generating AI-related words and phrases, and finally, composing a poem.

### Environment Requirements

- Python 3.8+
- Dependencies listed in main.py (tkinter, python-dotenv, requests, beautifulsoup4, openai, simplejson)

### Installation and Launch

# Install dependencies (example for Unix-like systems)
pip install python-dotenv requests beautifulsoup4 openai simplejson

# Launch the main module
python main.py


### How to Make Changes and Extend Functionality

To add new tools or agents, simply create the appropriate module in the tools or agents folder, following the existing structure. Don't forget to add the new tool or agent to the system initialization.

