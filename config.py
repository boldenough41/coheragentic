from dotenv import load_dotenv
import os

load_dotenv()  # Загружаем переменные окружения из .env файла

GROQ_API_KEY = os.getenv('GROQ_API_KEY')

# OpenAI API ключ
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
OPENAI_API_ORG = os.getenv('OPENAI_API_ORG')

def get_tuple_env(key):
    text = os.getenv(key)
    if text:
        return tuple(map(str, text.split(', ')))
    return None

VEXT_AI_ASSISTANT_API_KEY_PROJ = get_tuple_env('VEXT_AI_ASSISTANT_API_KEY_PROJ')

# Цель используемая для подсказки или задачи
OBJECTIVE = "Research recent AI news and write a poem about your findings in the style of Shakespeare."

# Флаг включения пользовательского ввода (True - включено, False - отключено)
USER_INPUT_ENABLED = False

# пока не используется
#SERPAPI_API_KEY = ""

REQUEST_HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36"
}

# Helper function to switch between enabled/disabled tools
def setup_variables():
    global websearch_var, user_input_var
    websearch_var = "[web-search] " if SERPAPI_API_KEY else ""
    user_input_var = "[user-input]" if USER_INPUT_ENABLED else ""

