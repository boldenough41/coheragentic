#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p "python3.withPackages(ps: with ps; [tkinter python-dotenv requests beautifulsoup4 openai simplejson])"

from ai_assistant_core import run as run_ai_assistant

if __name__ == "__main__":
    print("Запуск AI ассистента...")
    try:
        run_ai_assistant()
    except Exception as e:
        print(f"Произошла ошибка при выполнении: {e}")
    finally:
        print("AI ассистент завершил работу.")

