#!../python.sh

import requests
from bs4 import BeautifulSoup
import re
import openai
from typing import List, Dict, Optional
import os
import importlib.util
import unittest

AGENTS_DIR = os.path.dirname(__file__)
agents: Dict[str, Dict] = {}

def load_agents():
    global agents
    for dirname in os.listdir(AGENTS_DIR):
        if not dirname.endswith('.py'):
            module_name = dirname
            agent_path = os.path.join(AGENTS_DIR, dirname)
            spec = importlib.util.spec_from_file_location(module_name, os.path.join(agent_path, "__init__.py"))
            module = importlib.util.module_from_spec(spec)
            try:
                spec.loader.exec_module(module)
                if hasattr(module, 'agent_info'):
                    agents[module_name] = module.agent_info
            except Exception as e:
                print(f"Не удалось загрузить инструмент {module_name} из-за ошибки: {e}")

load_agents()

print(agents)

print(agents["thematic_chunker"]["make_prompt"]("my text"))

class Calc(unittest.TestCase):
    def test_add(self):
        self.assertEqual(add(10, 5), 15)
        self.assertEqual(add(-1, 1), 0)
        self.assertEqual(add(-1, -1), -2)

if __name__ == '__main__':
    unittest.main()

