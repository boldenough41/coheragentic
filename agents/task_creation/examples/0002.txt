EXAMPLE OBJECTIVE='''```Опишите какой важное для информатики значение имеет two conter machine от Марвина Мински и почему```'''
TASK LIST=[
  { "id": 1,
    "task": "Provide an overview of the two-counter machine model developed by Marvin Minsky and explain its significance in the field of computer science.",
    "tool": "text-completion",
    "dep_task_ids": []
  },
  { "id": 2,
    "task": "Describe the key features and capabilities of the two-counter machine, including its ability to simulate any Turing machine and its implications for computability theory.",
    "tool": "text-completion",
    "dep_task_ids": [1]
  },
  { "id": 3,
    "task": "Discuss how the two-counter machine model contributed to the development of fundamental concepts in computer science, such as the Church-Turing thesis and the theory of computational complexity.",
    "tool": "text-completion",
    "dep_task_ids": [2]
  },
  { "id": 4,
    "task": "Explain the practical applications and impact of the two-counter machine model on the field of computer science, such as its influence on the design of programming languages and the analysis of algorithms.",
    "tool": "text-completion",
    "dep_task_ids": [3]
  },
  {
    "id": 5,
    "task": "Summarize the key points from the previous tasks and discuss the overall significance of the two-counter machine model in the field of computer science, highlighting its contributions to the understanding of computability and the foundations of computer programming.",
    "tool": "text-completion",
    "dep_task_ids": [1, 2, 3, 4]
  }
]
