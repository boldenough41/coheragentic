#!../../python.sh

import sys
import os
import yaml
sys.path.append(os.path.abspath('../..'))

import unittest
from tools.text_completion import text_completion_tool
from __init__ import make_prompt, agent_info

from utils.dirty_chunker import dirty_chunker_util

from flow import perform_task

class Simple(unittest.TestCase):
    def test_simple(self):
        self.assertEqual("a", "a")

if __name__ == '__main__':
    #with open("../../ideas/llmos_01.txt", 'r', encoding='utf-8') as file:
    #with open("../../ideas/nextjs_crew_ai_01.txt", 'r', encoding='utf-8') as file:
    #with open("../../ideas/ilya_nvidia_subtitles.txt", 'r', encoding='utf-8') as file:
    #with open("../../ideas/agentic_ways_01.txt", 'r', encoding='utf-8') as file:
    #with open("../../ideas/llmos_01.txt", 'r', encoding='utf-8') as file:
    with open("../../ideas/crypto_openllm_01.txt", 'r', encoding='utf-8') as file:
        content = file.read()

    result = perform_task(text_completion_tool, content, 4096)

    print(result[0])
    print(result[1])


"""

    #dirty_slices = dirty_chunker_util(content)
    dirty_slices = dirty_chunker_util(content, 4096)

    a = make_prompt(dirty_slices[0])
    #print(a)

    b = text_completion_tool(a, agent_info['system'])
    #print("compl")
    #print(b)

    # шаг 1: находим индекс, где начинается нужный текст
    start_index = b.find("WISDOM_TAGS_FOR_TEXT")
    # шаг 2: делаем срез от этого индекса до конца строки
    if start_index != -1:  # проверяем, что подстрока была найдена
        subsequent_text = b[start_index:]
    else:
        subsequent_text = ""
        raise "не найден маркер"

    print(subsequent_text)

    # Находим начало интересующего нас блока и обрезаем все до '''yaml
    start_index = subsequent_text.find("THEME_NAME_AND_SNIPPET=```yaml")
    if start_index == -1:
        raise "THEME_NAME_AND_SNIPPET marker not found"
    else:
        # Обрезаем часть перед '''yaml и ищем закрывающие тройные кавычки
        start_index += len("THEME_NAME_AND_SNIPPET=```yaml")
        end_index = subsequent_text.find("```", start_index)
        if end_index == -1:
            raise "Closing triple quotes not found"
        else:
            yaml_content = subsequent_text[start_index:end_index].strip()

            # Парсим YAML
            try:
                data = yaml.safe_load(yaml_content)
                print("YAML")
                print(data)
                if isinstance(data, dict):
                    print("Number of elements:", len(data))
                    elems = len(data)
                    #for item in data:
                    #    print(item)
                else:
                    raise "Parsed data is not a list:"
            except yaml.YAMLError as exc:
                print("Error parsing YAML:", exc)

    if elems > 8:
        a = make_prompt(dirty_slices[0], subsequent_text, True)
        print(a)
    else:
        a = make_prompt(dirty_slices[1], subsequent_text)
        #print(a)

    b = text_completion_tool(a, agent_info['system'])
    #print("compl")
    #print(b)

    # шаг 1: находим индекс, где начинается нужный текст
    start_index = b.find("WISDOM_TAGS_FOR_TEXT")
    # шаг 2: делаем срез от этого индекса до конца строки
    if start_index != -1:  # проверяем, что подстрока была найдена
        subsequent_text = b[start_index:]
    else:
        subsequent_text = ""
        raise "не найден маркер"

    print(subsequent_text)
"""

"""
    unittest.main()
"""
